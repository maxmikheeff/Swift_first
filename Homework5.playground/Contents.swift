import UIKit

// MARK: - Задание 1

public class Shape {
    public func calculateArea() -> Double {
        fatalError("not implemented")
    }
    public func calculatePerimetr() -> Double {
        fatalError("not implemented")
    }
}

class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    override func calculateArea() -> Double {
        return height * width
    }
    
    override func calculatePerimetr() -> Double {
        return (height + width) * 2
    }
    
    init(height: Double, width: Double) {
        self.height = height
        self.width = width
    }
}

class Circle: Shape {
    private let radius: Double
    
    override func calculateArea() -> Double {
        return 3.14 * (radius * radius)
    }
    
    override func calculatePerimetr() -> Double {
        return 2 * 3.14 * radius
    }
    
    init(radius: Double) {
        self.radius = radius
    }
}

class Square: Shape {
    private let side: Double
    
    override func calculateArea() -> Double {
        return side * side
    }
    
    override func calculatePerimetr() -> Double {
        return side * 4
    }
    
    init(side: Double) {
        self.side = side
    }
}

var area: Double
var perimeter: Double
var shapes: [Shape] = [Rectangle(height: 4, width: 5), Circle(radius: 6), Square(side: 7)]

for i in shapes {
    area = i.calculateArea()
    print(area)
    perimeter = i.calculatePerimetr()
    print(perimeter)
}

// MARK: - Задание 2

func findIndexWithGenerics<T: Equatable>(of valueToFind: T, in array: [T]) -> Int? {
    for value in array {
        if value == valueToFind {
            return array.firstIndex(of: valueToFind)
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]

if let foundIndex = findIndexWithGenerics(of: "лама", in: arrayOfString) {
    print("index is \(foundIndex)")
}

let arrayOfDouble = [1.1, 2.2, 3.3, 4.4, 5.5]

if let foundIndex = findIndexWithGenerics(of: 4.4, in: arrayOfDouble) {
    print("index is \(foundIndex)")
}

let arrayOfInt = [11, 22, 33, 44, 55]

if let foundIndex = findIndexWithGenerics(of: 55, in: arrayOfInt) {
    print("index is \(foundIndex)")
}
