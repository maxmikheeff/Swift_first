import UIKit

// MARK: - Задание 1

func makeBuffer() -> ((String) -> String) {
    
    var data = ""
    
    func buffer(_ stroka: String) -> String {
        if stroka.isEmpty {
            return data
        } else {
            data.append(stroka)
            return ""
        }
    }
    return buffer
}

var buffer = makeBuffer()
