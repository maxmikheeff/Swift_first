import UIKit

struct Device {
    class IPhone {
        var name: String
        var screenSize: CGSize
        var screenDiagonal: Double
        var scaleFactor: Int
        
        init(name: String,
             screenSize: CGSize,
             screenDiagonal: Double,
             scaleFactor: Int) {
            self.name = name
            self.screenSize = screenSize
            self.screenDiagonal = screenDiagonal
            self.scaleFactor = scaleFactor
        }
        
        func physicalSize() -> CGSize {
            return CGSize(
                width: screenSize.width * CGFloat(scaleFactor),
                height: screenSize.height * CGFloat(scaleFactor)
            )
        }
    }
    
    class IPhone14Pro: IPhone {
        override init(name: String = "iPhone 14 Pro",
                      screenSize: CGSize = CGSize(width: 393, height: 852),
                      screenDiagonal: Double = 6.1,
                      scaleFactor: Int = 3) {
            super.init(name: name, screenSize: screenSize, screenDiagonal: screenDiagonal, scaleFactor: scaleFactor)
        }
    }
    
    class IPhoneXR: IPhone {
        override init(name: String = "iPhoneXR",
                      screenSize: CGSize = CGSize(width: 414, height: 896),
                      screenDiagonal: Double = 6.06,
                      scaleFactor: Int = 2) {
            super.init(name: name, screenSize: screenSize, screenDiagonal: screenDiagonal, scaleFactor: scaleFactor)
        }
    }
    
    class IPadPro: IPhone {
        override init(name: String = "iPadPro",
                      screenSize: CGSize = CGSize(width: 834, height: 1194),
                      screenDiagonal: Double = 11,
                      scaleFactor: Int = 2) {
            super.init(name: name, screenSize: screenSize, screenDiagonal: screenDiagonal, scaleFactor: scaleFactor)
        }
    }
}


Device.IPhone14Pro().physicalSize()
Device.IPhoneXR().physicalSize()
Device.IPadPro().physicalSize()

