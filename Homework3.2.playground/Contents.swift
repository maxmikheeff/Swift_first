import UIKit

// MARK: - Задание 2

func checkPrimeNumber(number: Int) -> Bool {
    
    if number <= 1 {
        return false
    }
  
    for i in 2 ..< number {
        if number % i == 0 {
            return false
        }
    }
    return true
}

checkPrimeNumber(number: 11)
