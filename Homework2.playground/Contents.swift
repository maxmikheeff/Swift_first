import UIKit

// MARK: - Задание 1

var milkmanPhrase = "Молоко - это полезно"
print(milkmanPhrase)

// MARK: - Задание 2

//let milkPrice: Int = "Три рубля"
var milkPrice: Double = 3

// MARK: - Задание 3

milkPrice = 4.20 // Поменял константу в 11 строке на переменную, чтобы можно было присвоить новое значение. Так же поменял тип Int на Double, чтобы присвоить число с точкой

// MARK: - Задание 4

var milkBottleCount: Int? = 20
var profit: Double = 0.0

profit = milkPrice * Double(milkBottleCount!)
print(profit)

// MARK: - Задание 5

var employeesList = [String]()
//employeesList = ["Иван","Петр","Геннадий","Андрей","Марфа"]

func addEmployeesToArray(employee: String) {
    employeesList.append(employee)
}
addEmployeesToArray(employee: "Иван")
addEmployeesToArray(employee: "Петр")
addEmployeesToArray(employee: "Геннадий")
addEmployeesToArray(employee: "Андрей")
addEmployeesToArray(employee: "Марфа")

print(employeesList)

// MARK: - Задание 6

var isEveryoneWorkHard = false //1 пункт
var workingHours = 30  //2 и 5 пункт: тут нужно было изначально задать 40, но в 5 пункте нужно это значение изменить

if workingHours >= 40 { //3 пункт
    isEveryoneWorkHard = true
}
else {
    isEveryoneWorkHard = false
}

print(isEveryoneWorkHard) // 4 и 6 пункт: два раза нужно вывести одно и тоже, но все изменения в 33 строке по сути

// MARK: - Дополнительное задание

//if let milkBottleCount = milkBottleCount {
//    profit = milkPrice * Double(milkBottleCount)
//}
//
//стоит избегать принудительного извлечения, так как если в опционале не окажется значения, то это приведет к ошибке выполнения программы
//
//milkBottleCount != nil ? profit = milkPrice * Double(milkBottleCount!) : print("Ошибка") // пример как еще можно извлечь значение безопасно
